<?php

/* themes/custom/food_recipes/template/page--404.html.twig */
class __TwigTemplate_d2477162932756ec2400b3044ae4c7642361146f4c03cb21f946ab739657b992 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 62);
        $filters = array("t" => 55);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array('t'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 53
        echo "<div id=\"page-wrapper\">
        <div id=\"page\">
          <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
        // line 55
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Site header")));
        echo "\">
            <div class=\"section layout-container clearfix\">
              ";
        // line 57
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "secondary_menu", array()), "html", null, true));
        echo "
              ";
        // line 58
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
        echo "
              ";
        // line 59
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "primary_menu", array()), "html", null, true));
        echo "
            </div>
          </header>
          ";
        // line 62
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", array())) {
            // line 63
            echo "            <div class=\"highlighted\">
              <aside class=\"layout-container section clearfix\" role=\"complementary\">
                ";
            // line 65
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "highlighted", array()), "html", null, true));
            echo "
              </aside>
            </div>
          ";
        }
        // line 69
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "featured_top", array())) {
            // line 70
            echo "            <div class=\"featured-top\">
              <aside class=\"featured-top__inner section layout-container clearfix\" role=\"complementary\">
                ";
            // line 72
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "featured_top", array()), "html", null, true));
            echo "
              </aside>
            </div>
          ";
        }
        // line 76
        echo "          <div id=\"main-wrapper\" class=\"layout-main-wrapper layout-container clearfix\">
            <div id=\"main\" class=\"layout-main clearfix\">
              ";
        // line 78
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "breadcrumb", array()), "html", null, true));
        echo "
              <main id=\"content\" class=\"column main-content\" role=\"main\">
                <section class=\"section\">
                  <a id=\"main-content\" tabindex=\"-1\"></a>

                    <div class=\"page--404--not--found\">
                        <p>Ops!!! Essa página não foi encontrada...</p>
                        <img src=\"/themes/custom/food_recipes/assets/images/404.png\" />
                    </div>

                </section>
              </main>
              ";
        // line 90
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", array())) {
            // line 91
            echo "                <div id=\"sidebar-first\" class=\"column sidebar\">
                  <aside class=\"section\" role=\"complementary\">
                    ";
            // line 93
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_first", array()), "html", null, true));
            echo "
                  </aside>
                </div>
              ";
        }
        // line 97
        echo "              ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", array())) {
            // line 98
            echo "                <div id=\"sidebar-second\" class=\"column sidebar\">
                  <aside class=\"section\" role=\"complementary\">
                    ";
            // line 100
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "sidebar_second", array()), "html", null, true));
            echo "
                  </aside>
                </div>
              ";
        }
        // line 104
        echo "            </div>
          </div>
          ";
        // line 106
        if ((($this->getAttribute(($context["page"] ?? null), "featured_bottom_first", array()) || $this->getAttribute(($context["page"] ?? null), "featured_bottom_second", array())) || $this->getAttribute(($context["page"] ?? null), "featured_bottom_third", array()))) {
            // line 107
            echo "            <div class=\"featured-bottom\">
              <aside class=\"layout-container clearfix\" role=\"complementary\">
                ";
            // line 109
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "featured_bottom_first", array()), "html", null, true));
            echo "
                ";
            // line 110
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "featured_bottom_second", array()), "html", null, true));
            echo "
                ";
            // line 111
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "featured_bottom_third", array()), "html", null, true));
            echo "
              </aside>
            </div>
          ";
        }
        // line 115
        echo "          <footer class=\"site-footer\">
            <div class=\"layout-container\">
              ";
        // line 117
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", array()) || $this->getAttribute(($context["page"] ?? null), "footer_second", array())) || $this->getAttribute(($context["page"] ?? null), "footer_third", array())) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", array()))) {
            // line 118
            echo "                <div class=\"site-footer__top clearfix\">
                  ";
            // line 119
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_first", array()), "html", null, true));
            echo "
                  ";
            // line 120
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_second", array()), "html", null, true));
            echo "
                  ";
            // line 121
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_third", array()), "html", null, true));
            echo "
                  ";
            // line 122
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_fourth", array()), "html", null, true));
            echo "
                </div>
              ";
        }
        // line 125
        echo "              ";
        if ($this->getAttribute(($context["page"] ?? null), "footer_fifth", array())) {
            // line 126
            echo "                <div class=\"site-footer__bottom\">
                  ";
            // line 127
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_fifth", array()), "html", null, true));
            echo "
                </div>
              ";
        }
        // line 130
        echo "            </div>
          </footer>
        </div>
      </div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/food_recipes/template/page--404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 130,  197 => 127,  194 => 126,  191 => 125,  185 => 122,  181 => 121,  177 => 120,  173 => 119,  170 => 118,  168 => 117,  164 => 115,  157 => 111,  153 => 110,  149 => 109,  145 => 107,  143 => 106,  139 => 104,  132 => 100,  128 => 98,  125 => 97,  118 => 93,  114 => 91,  112 => 90,  97 => 78,  93 => 76,  86 => 72,  82 => 70,  79 => 69,  72 => 65,  68 => 63,  66 => 62,  60 => 59,  56 => 58,  52 => 57,  47 => 55,  43 => 53,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/food_recipes/template/page--404.html.twig", "/var/www/html/drupal-learning/themes/custom/food_recipes/template/page--404.html.twig");
    }
}
