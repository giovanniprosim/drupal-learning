const gulp = require('gulp')
const sass = require('gulp-sass')
const sourcempas = require('gulp-sourcemaps')
const cleanCSS = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer')
const rename = require('gulp-rename')
const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const uglify = require('gulp-uglify')
const browserify = require('browserify')
const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')

const run = require('gulp-run-command').default;

gulp.task('sass', () => {
    return gulp.src('./src/scss/index.scss')
        .pipe(sourcempas.init())
        .pipe(sass.sync()
        .on('error', notify.onError((error) => {
            return `Compile Error: ${error.message}`
        })))
        .pipe(autoprefixer({
        browsers: ['last 5 versions', 'ie >= 8']
        }))
        .pipe(plumber())
        .pipe(cleanCSS({ compatibility: 'ie8' }, { debug: true }, (details) => {
        console.log(`${details.name}: ${details.stats.originalSize}`)
        console.log(`${details.name}: ${details.stats.minifiedSize}`)
        }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcempas.write())
        .pipe(gulp.dest('./dist/css'))
})

gulp.task('scripts', () => {
    return browserify('./src/js/index.js')
        .transform('babelify', {
        presets: ['@babel/preset-env']
        }).bundle()
        .pipe(plumber())
        .pipe(source('index.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/js'))
})

gulp.task('watch', () => {

    const scssFolder = './src/scss/**/*.scss';
    const jsFolder = './src/js/**/*.js';

    gulp.watch(scssFolder, ['sass'])
    gulp.watch(jsFolder, ['scripts'])

    gulp.watch([scssFolder, jsFolder], ['clean'])

})

gulp.task('clean', run('../../../vendor/bin/drush cr'))

gulp.task('build', ['sass', 'scripts', 'clean'])
gulp.task('default', ['build', 'watch'])