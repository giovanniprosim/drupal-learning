import PagePart from './_part_page.js'

class SliderShow extends PagePart{

    constructor() {
        super()
        this.currentId = 0
        
    }

    init() {

        let bullets = document.querySelectorAll('.views-slideshow-pager-field-processed li')

        if(bullets && bullets.length > 0) {
            
            this.toogleBullet(this.currentId)

            bullets.forEach( (b,i) => {
                b.onclick = () => {
                    this.toogleSlide(this.currentId)
                    this.toogleSlide(i)
                    this.currentId = i
                }
    
            })

        }

    }

    toogleSlide(id) {
        let slide = document.querySelector('#views_slideshow_cycle_div_frontpage-page_1_'+ id)
        slide.classList.toggle('views_slideshow_cycle_hidden')
    }

    toogleBullet(id) {
        let bullet = document.querySelector('#views_slideshow_pager_field_item_bottom_frontpage-page_1_'+ id)
        bullet.classList.toggle('active')
    }

}

export default new SliderShow