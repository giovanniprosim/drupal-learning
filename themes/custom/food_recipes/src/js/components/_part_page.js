class PartPage {

    constructor() {

        if(!window.allOnLoadFunctions){
            window.allOnLoadFunctions = []
        }

        window.allOnLoadFunctions.push(this)
    
    }

    init(){}

    resize(){}

}

export default PartPage