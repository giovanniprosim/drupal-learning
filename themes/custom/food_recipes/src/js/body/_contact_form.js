import PartPage from '../components/_part_page.js'

class ContactForm extends PartPage {
    
    init() {

        let helpText = document.querySelector('.form-type-managed-file .description')

        if(helpText) {
            helpText.innerHTML = helpText.innerHTML.replace(/<br>/gi, ' ')
        }        

    }

}

export default new ContactForm