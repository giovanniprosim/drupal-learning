import PartPage from '../components/_part_page.js'

class Banner extends PartPage {

    constructor() {

        super()

        this.banner = document.querySelector('.block-block-content .content')

        this.mainTxt = document.querySelector('.field--name-field-main-message')
        this.secundaryTxt = document.querySelector('.field--name-field-secundary-message')

        this.bannerImg = document.querySelector('.field--name-field-background-image img')
        if(this.bannerImg) {
            this.bannerImgUrl = this.bannerImg.getAttribute('src')
            this.allDivRecipes = document.querySelectorAll('.views_slideshow_cycle_main div.field-content')
        }

    }
    
    init() {

        if(this.banner) {
            
            this.banner.style.backgroundImage = 'url(' + this.bannerImgUrl + ')'
            
            this.bannerImg.remove()

            this.allDivRecipes && this.allDivRecipes.forEach(d => 
                {
                    const imgUrl = d.querySelector('img').getAttribute('src')
                    d.style.backgroundImage = this.setBackgroundImage(imgUrl)
                }
            )
        }

    }

    setBackgroundImage(url) {
        return 'linear-gradient(to bottom, rgba(1,1,1,0.2) 0%,rgba(1,1,1,0.2) 100%), url('+url+')'
    }

}

export default new Banner