import PartPage from '../components/_part_page.js'

class Branding extends PartPage {

    init() {

        let brandingBlock = document.querySelector('.block-system-branding-block')
        let img = document.querySelector('.site-branding__logo')

        let newImg = img.cloneNode(true)        

        newImg.style.transform = 'scaleX(-1)'
        
        brandingBlock.appendChild(newImg)

    }

}

export default new Branding