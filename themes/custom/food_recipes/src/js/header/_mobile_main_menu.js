import PartPage from '../components/_part_page.js'

class MobileMainMenu extends PartPage{

    constructor() {

        super()

        this.searchField = document.querySelector('.search-block-form')   
        this.menu = document.querySelector('.region-primary-menu ul.menu')     
    }

    init() {

        if(window.innerWidth < 768) {

            let li = document.createElement("li")
            li.classList.add('menu-item')

            this.menu.prepend(li)
            document.querySelector('.region-primary-menu ul.menu li').prepend(this.searchField)

        }

    }

}

export default new MobileMainMenu